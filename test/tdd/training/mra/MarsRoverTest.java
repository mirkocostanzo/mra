package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	//User story 1
	public void testPlanet() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<String>();
			
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		boolean obstacle = rover.planetContainsObstacleAt(2, 3);
			
		assertEquals(true, obstacle);
	 }
	
	@Test
	//User story 2
	public void testLanding() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<String>();
			
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "";
		
		assertEquals("(0,0,N)", rover.executeCommand(commandString));
	}
	
	@Test
	//User story 3
	public void testTurning() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<String>();
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "";
		assertEquals("(0,0,N)", rover.executeCommand(commandString));
		
		
		commandString = "r";
		assertEquals("(0,0,E)", rover.executeCommand(commandString));
		
		
		commandString = "r";
		assertEquals("(0,0,S)", rover.executeCommand(commandString));
		
		commandString = "l";
		assertEquals("(0,0,E)", rover.executeCommand(commandString));
		
		commandString = "l";
		assertEquals("(0,0,N)", rover.executeCommand(commandString));
		
		commandString = "l";
		assertEquals("(0,0,W)", rover.executeCommand(commandString));
		
		commandString = "";
		assertEquals("(0,0,N)", rover.executeCommand(commandString));
	}
	
	@Test
	//User story 4
	public void testMovingForward() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<String>();
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "f";
		assertEquals("(0,1,N)", rover.executeCommand(commandString));
		
		commandString = "f";
		assertEquals("(0,2,N)", rover.executeCommand(commandString));
		
		commandString = "r";
		assertEquals("(0,2,E)", rover.executeCommand(commandString));
		
		commandString = "f";
		assertEquals("(1,2,E)", rover.executeCommand(commandString));
	}

	@Test
	//User story 5
	public void testMovingBackward() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<String>();
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "f";
		assertEquals("(0,1,N)", rover.executeCommand(commandString));
		
		commandString = "f";
		assertEquals("(0,2,N)", rover.executeCommand(commandString));
		
		commandString = "b";
		assertEquals("(0,1,N)", rover.executeCommand(commandString));
		
		commandString = "b";
		assertEquals("(0,0,N)", rover.executeCommand(commandString));
	}
	
	@Test
	//User story 6
	public void testMovingCombined() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<String>();
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "ffrff";
		assertEquals("(2,2,E)", rover.executeCommand(commandString));
	}
	
	@Test
	//User story 7
	public void testWrapping() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<String>();
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "b";
		assertEquals("(0,9,N)", rover.executeCommand(commandString));
		
		commandString = "f";
		assertEquals("(0,0,N)", rover.executeCommand(commandString));
		
		commandString = "l";
		assertEquals("(0,0,W)", rover.executeCommand(commandString));
		
		commandString = "f";
		assertEquals("(9,0,W)", rover.executeCommand(commandString));
	}
	
	@Test
	//User story 8
	public void testSingleObstacle() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<String>();
		planetObstacles.add("(2,2)");
		
	MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "ffrfff";
		assertEquals("(1,2,E)(2,2)", rover.executeCommand(commandString));
	}
	
	@Test
	//User story 9
	public void testMultipleObstacles() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<String>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(2,1)");
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "ffrfffrflf";
		assertEquals("(1,1,E)(2,2)(2,1)", rover.executeCommand(commandString));
	}
		
	@Test
	//User story 10
	public void testWrappingAndObstacles() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<String>();
		planetObstacles.add("(0,9)");
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "b";
		assertEquals("(0,0,N)(0,9)", rover.executeCommand(commandString));
	}
	
	@Test
	//User story 11
	public void testTourAroundThePlanet() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<String>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(0,5)");
		planetObstacles.add("(5,0)");
		
		MarsRover rover = new MarsRover(6, 6, planetObstacles);
		
		String commandString = "ffrfffrbbblllfrfrbbl";
		assertEquals("(0,0,N)(2,2)(0,5)(5,0)", rover.executeCommand(commandString));
	}
}
